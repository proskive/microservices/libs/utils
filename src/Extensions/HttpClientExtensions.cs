﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProSkive.Lib.Utils.Extensions
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PatchAsync(this HttpClient client, Uri requestUri, HttpContent content)
        {            
            var patchMethod = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(patchMethod, requestUri)
            {
                Content =  content
            };

            return client.SendAsync(request);
        }
        
        public static Task<HttpResponseMessage> PatchAsync(this HttpClient client, string requestUri, HttpContent content)
        {            
            var patchMethod = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(patchMethod, requestUri)
            {
                Content =  content
            };

            return client.SendAsync(request);
        }
    }
}
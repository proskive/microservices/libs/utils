﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProSkive.Lib.Utils.Attributes.Validation
{
    // Source: http://www.technofattie.com/2011/10/05/recursive-validation-using-dataannotations.html
    public class ValidateNestedObjectAttribute : ValidationAttribute {
        
        public override bool IsValid(object value)
        {
            var validationContext = new ValidationContext(this, null, null);
            var validationResult = IsValid(value, validationContext);

            return validationResult == ValidationResult.Success;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
            if (value is null)
                return ValidationResult.Success;
            
            var results = new List<ValidationResult>();

            if (value is ICollection collection)
            {
                foreach (var element in collection)
                {
                    var context = new ValidationContext(element, null, null);
                    Validator.TryValidateObject(element, context, results, true);
                }
            }
            else
            {
                var context = new ValidationContext(value, null, null);
                Validator.TryValidateObject(value, context, results, true);
            }
            

            if (results.Count == 0) 
                return ValidationResult.Success;
            
            var compositeResults = new CompositeValidationResult($"Validation for {validationContext.DisplayName} failed!");
            results.ForEach(compositeResults.AddResult);

            return compositeResults;

        }
    }

    public class CompositeValidationResult : ValidationResult {
        private readonly List<ValidationResult> results = new List<ValidationResult>();

        public IEnumerable<ValidationResult> Results => results;

        public CompositeValidationResult(string errorMessage) : base(errorMessage) {}
        public CompositeValidationResult(string errorMessage, IEnumerable<string> memberNames) : base(errorMessage, memberNames) {}
        protected CompositeValidationResult(ValidationResult validationResult) : base(validationResult) {}

        public void AddResult(ValidationResult validationResult) {
            results.Add(validationResult);
        }
    }
}
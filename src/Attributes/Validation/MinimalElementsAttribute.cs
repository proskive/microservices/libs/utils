﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace ProSkive.Lib.Utils.Attributes.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class MinimalElementsAttribute : ValidationAttribute
    {
        private readonly int minElements;
        public MinimalElementsAttribute(int minElements)
        {
            this.minElements = minElements;
        }

        public override bool IsValid(object value)
        {
            if (value is IList list)
            {
                return list.Count >= minElements;
            }
            return false;
        }
    }
}
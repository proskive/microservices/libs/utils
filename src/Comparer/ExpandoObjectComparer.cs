﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using KellermanSoftware.CompareNetObjects;
using KellermanSoftware.CompareNetObjects.TypeComparers;

namespace ProSkive.Lib.Utils.Comparer
{
    public class ExpandoObjectComparer : BaseTypeComparer
    {
        public ExpandoObjectComparer(RootComparer rootComparer) : base(rootComparer)
        {
        }

        public override bool IsTypeMatch(Type type1, Type type2)
        {
            return type1 == typeof(ExpandoObject) && type2 == typeof(ExpandoObject);
        }

        public override void CompareType(CompareParms parms)
        {
            var expandoDict1 = new Dictionary<string, object>((ExpandoObject) parms.Object1);
            var expandoDict2 = new Dictionary<string, object>((ExpandoObject) parms.Object2);

            parms.Object1 = expandoDict1;
            parms.Object1Type = expandoDict1.GetType();
            parms.Object2 = expandoDict2;
            parms.Object2Type = expandoDict2.GetType();
            
            this.RootComparer.Compare(parms);
        }
    }
}
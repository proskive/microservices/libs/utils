﻿using System.ComponentModel.DataAnnotations;
using NUnit.Framework;
using ProSkive.Lib.Utils.Attributes.Validation;

namespace ProSkive.Tests.Lib.Utils.UnitTests.Attributes.Validation
{
    [TestFixture]
    [Category("Unit"), Category("Attributes"), Category("ValidateNestedObjectAttribute")]
    public class ValidateNestedObjectAttributeTests
    {
        private class NestedObject
        {
            [Required] public string Name { get; set; }
        }

        [Test]
        public void IsValid_NotSatisfyNestedObject_IsNotValid()
        {
            // Arrange
            var nestedObject = new NestedObject()
            {
                Name = null
            };

            var attribute = new ValidateNestedObjectAttribute();

            // Act
            var valid = attribute.IsValid(nestedObject);

            // Assert
            Assert.That(valid, Is.False);
        }
        
        [Test]
        public void IsValid_SatisfyNestedObject_IsValid()
        {
            // Arrange
            var nestedObject = new NestedObject()
            {
                Name = "Not Null"
            };

            var attribute = new ValidateNestedObjectAttribute();

            // Act
            var valid = attribute.IsValid(nestedObject);

            // Assert
            Assert.That(valid, Is.True);
        }
        
    }
}
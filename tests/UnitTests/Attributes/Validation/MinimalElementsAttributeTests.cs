﻿using NUnit.Framework;
using ProSkive.Lib.Utils.Attributes.Validation;

namespace ProSkive.Tests.Lib.Utils.UnitTests.Attributes.Validation
{
    [TestFixture]
    [Category("Unit"), Category("Attributes"), Category("MinimalElementsAttribute")]
    public class MinimalElementsAttributeTests
    {
        [Test]
        public void IsValid_LessThanMinimum_NotValid()
        {
            // Arrange
            var array = new string[]
            {
                "one element"
            };

            var attribute = new MinimalElementsAttribute(2);

            // Act
            var valid = attribute.IsValid(array);

            // Assert
            Assert.That(valid, Is.False);
        }
        
        [Test]
        public void IsValid_EqualMinimum_Valid()
        {
            // Arrange
            var array = new string[]
            {
                "one element",
                "two elemens"
            };

            var attribute = new MinimalElementsAttribute(2);

            // Act
            var valid = attribute.IsValid(array);

            // Assert
            Assert.That(valid, Is.True);
        }
        
        [Test]
        public void IsValid_MoreThanMinimum_Valid()
        {
            // Arrange
            var array = new string[]
            {
                "one element",
                "two elemens",
                "three elements"
            };

            var attribute = new MinimalElementsAttribute(2);

            // Act
            var valid = attribute.IsValid(array);

            // Assert
            Assert.That(valid, Is.True);
        }
    }
}
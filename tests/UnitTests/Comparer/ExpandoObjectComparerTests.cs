﻿using System.Dynamic;
using System.Linq;
using KellermanSoftware.CompareNetObjects;
using NUnit.Framework;
using ProSkive.Lib.Utils.Comparer;

namespace ProSkive.Tests.Lib.Utils.UnitTests.Comparer
{
    [TestFixture]
    [Category("Unit"), Category("Comparer"), Category("ExpandoObjectComparer")]
    public class ExpandoObjectComparerTests
    {
        private CompareLogic compareLogic;
        
        [SetUp]
        public void SetUp()
        {
            compareLogic = new CompareLogic();
            compareLogic.Config.CustomComparers.Add(new ExpandoObjectComparer(RootComparerFactory.GetRootComparer()));
        }
        
        [Test]
        public void IsTypeMatch_ObjOneIsNotExpando_DifferenceAsDifferentTypes()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.String = "I'm a string of an ExpanoObject";
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: anonymous
            var obj2 = new
            {
                String = "I'm a string of an object"
            };
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.EqualTo(MessagePrefix.DifferentTypes));
        }
        
        [Test]
        public void IsTypeMatch_ObjTwoIsNotExpando_DifferenceAsDifferentTypes()
        {
            // Arrange
            // -- obj1: anonymous
            var obj1 = new
            {
                String = "I'm a string of an object"
            };
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.String = "I'm a string of an ExpanoObject";
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.EqualTo(MessagePrefix.DifferentTypes));
        }
        
        // String - Tests
        [Test]
        public void CompareType_StringIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.String = "I'm a equal string";
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.String = "I'm a equal string";
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_StringIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.String = "I'm a equal string";
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.String = "I'm not a equal string";
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Bool Tests
        [Test]
        public void CompareType_BoolIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Bool = true;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Bool = true;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_BoolIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Bool = true;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Bool = false;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Int - Tests
        [Test]
        public void CompareType_IntIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Int = 1;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Int = 1;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_IntIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Int = 1;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Int = 2;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Long - Tests
        [Test]
        public void CompareType_LongIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Long = 1523836800000L;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Long = 1523836800000L;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_LongIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Long = 1523836800000L;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Long = 9523836800001L;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Double - Tests
        [Test]
        public void CompareType_DoubleIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Double = 1.0;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Double = 1.0;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_DoubleIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Double = 1.0;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Double = 2.0;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Array - Tests
        [Test]
        public void CompareType_ArrayIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Array = new object[] { "hello", "equal", "array" };
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Array = new object[] { "hello", "equal", "array" };
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_ArrayIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.Array = new object[] { "hello", "equal", "array" };
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.Array = new object[] { "hello", "not", "equal", "array" };
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        // Nested Objects - Tests
        [Test]
        public void CompareType_NestedAnonymousObjectIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.NestedAnonymousObject = new
            {
                String = "String of nested anonymous object"
            };
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.NestedAnonymousObject = new
            {
                String = "String of nested anonymous object"
            };
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_NestedAnonymousObjectIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic expando1 = new ExpandoObject();
            expando1.NestedAnonymousObject = new
            {
                String = "String of nested anonymous object"
            };
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic expando2 = new ExpandoObject();
            expando2.NestedAnonymousObject = new
            {
                String = "Different String of nested anonymous object"
            };
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
        
        [Test]
        public void CompareType_NestedExpandoObjectIsEqual_ResultAreEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic nestedExpandoObject1 = new ExpandoObject();
            nestedExpandoObject1.String = "String of nested expando object";
            var nestedObj1 = (ExpandoObject) nestedExpandoObject1;
            
            dynamic expando1 = new ExpandoObject();
            expando1.NestedExpandoObject = nestedExpandoObject1;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic nestedExpandoObject2 = new ExpandoObject();
            nestedExpandoObject2.String = "String of nested expando object";
            var nestedObj2 = (ExpandoObject) nestedExpandoObject2;
            
            dynamic expando2 = new ExpandoObject();
            expando2.NestedExpandoObject = nestedExpandoObject2;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            Assert.That(result.AreEqual, Is.True);
        }
        
        [Test]
        public void CompareType_NestedExpandoObjectIsNotEqual_ResultAreNotEqual()
        {
            // Arrange
            // -- obj1: expando
            dynamic nestedExpandoObject1 = new ExpandoObject();
            nestedExpandoObject1.String = "String of nested expando object";
            var nestedObj1 = (ExpandoObject) nestedExpandoObject1;
            
            dynamic expando1 = new ExpandoObject();
            expando1.NestedExpandoObject = nestedExpandoObject1;
            var obj1 = (ExpandoObject) expando1;
            
            // -- obj2: expando
            dynamic nestedExpandoObject2 = new ExpandoObject();
            nestedExpandoObject2.String = "Different String of nested expando object";
            var nestedObj2 = (ExpandoObject) nestedExpandoObject2;
            
            dynamic expando2 = new ExpandoObject();
            expando2.NestedExpandoObject = nestedExpandoObject2;
            var obj2 = (ExpandoObject) expando2;
            
            // Act
            var result = compareLogic.Compare(obj1, obj2);

            // Assert
            var difference = result.Differences.FirstOrDefault();
            Assert.That(difference?.MessagePrefix, Is.Null);
            Assert.That(result.AreEqual, Is.False);
        }
    }
}